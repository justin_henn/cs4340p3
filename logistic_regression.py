#Justin Henn
#Project 3
#Logistic Regression

#imported libraries
import matplotlib.pyplot as plt
import numpy as np 
from math import exp

#cost function
def cost(i, x, w):
    h = 0
    for z in range(len(x[i])):
        h += w[z] * x[i][z]
    return 1.0 / (1.0 + exp(-h))

#regression function
def regression(x, y, w, eta):
    w_temp = [.0] * 2
    m = len(y)
    for i in range(m):
        sse = 0
        h = cost(i, x, w)
        error = y[i] - h
        sse += error**2
        for z in range(len(x[i])):
            w_temp[z] += (1/m) * x[i][z] * (h - y[i])
        w[0] = w[0] - (eta * w_temp[0])
        w[1] = w[1] - (eta * w_temp[1])
    return sse

eta = .01
epoch = 20000

#data sets
x = [[1, 1],
     [1, 2],
     [1, 3],
     [1, 4],
     [1, 5],
     [1, 6],
     [1, 7],
     [1, 8]]
y = [0, 1, 0, 1, 0, 1, 1, 1]

w = [0]*2

temp_error = 0
same_time = 0

#run regression
for i in range(epoch):
    sse = regression(x, y, w, eta)
    if temp_error == sse:
        print(i)
        break
    else:
        temp_error = sse

#print and graph results
print('w_0 = %.4f, w_1 = %.4f' % (w[0],w[1]))

for i,z in enumerate(x):
    plt.scatter(x[i][1], y[i], s=120, linewidths=2)
x1 = np.linspace(-10, 10)
plt.plot(x1, (1.0/(1.0+np.exp((w[0]*x1)+w[1]))))
plt.show()
